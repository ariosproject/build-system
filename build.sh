#!/bin/bash

if [ -z "$1" ]
  then
    echo "No argument supplied"
    echo "Usage : build.sh config [arch]"
    echo "Availabe architectures : i386,amd64"
    echo -n "Availabe Configs : "
    ls Dists|sed ':a;N;$!ba;s/\n/,/g'
    exit
fi

rm Build/root.tar
rm Build/root-config.tar
rm Build/ADD
rm Build/REMOVE
rm Build/PPA
rm Build/MANUAL.sh
rm Build/aconf.sh

#create tarball form base and selected config
echo "create tarball form base and selected config ..."

#base
cd Dists/base/root
tar -cvf root.tar *
mv root.tar ../../../Build

if [ "$1" != "base" ]; then
	#config
	#TODO : check for config existance
	cd ../../$1/root
	tar -cvf root-config.tar *
	mv root-config.tar ../../../Build
fi

echo "moving base rules to build dir and append config rules to base rules ..."
cd ../../base
cp ADD REMOVE PPA MANUAL.sh ../../Build

#append
if [ "$1" != "base" ]; then
	cd ../$1
	cat ADD >> ../../Build/ADD
	cat REMOVE >> ../../Build/REMOVE
	cat PPA >> ../../Build/PPA
	cat MANUAL.sh >> ../../Build/MANUAL.sh
fi
	#make changes
	cd ../../Build/
	echo "apt-fast install -f -y" |  tr '\n' ' ' > ADD.tmp
	cat ADD  |  grep  -v "#"  | tr '\n' ' '>> ADD.tmp
	echo "" >> ADD.tmp

	echo "apt-get -f -y purge" |  tr '\n' ' ' > REMOVE.tmp
	cat REMOVE  |  grep  -v "#"  | tr '\n' ' '>> REMOVE.tmp
	echo "" >> REMOVE.tmp

	cat PPA    |  grep  -v "#" | xargs -I{} echo "add-apt-repository -y ppa:{}" > PPA.tmp
	mv ADD.tmp ADD
	mv REMOVE.tmp REMOVE
	mv PPA.tmp PPA

#add to autoconfig file	
	while read line; do 
		if [ "$line" == "{add}" ] ; then
			while read -r addline; do 
				echo "$addline"  >> aconf.sh
			done < ADD
		elif [ "$line" == "{remove}" ] ; then
			while read -r  remline; do 
				echo "$remline"  >> aconf.sh
			done < REMOVE
		elif [ "$line" == "{ppa}" ] ; then
			while read -r  ppaline; do 
				echo "$ppaline"  >> aconf.sh
			done < PPA
		elif [ "$line" == "{manual}" ] ; then
			while read -r  manline; do 
				echo "$manline"  >> aconf.sh
			done < MANUAL.sh
		else
			echo $line >> aconf.sh
		fi
	done < aconf.sh.pattern

rm ADD
rm REMOVE
rm PPA
rm MANUAL.sh

echo "start build script"
sudo ./abuild.sh --$2
