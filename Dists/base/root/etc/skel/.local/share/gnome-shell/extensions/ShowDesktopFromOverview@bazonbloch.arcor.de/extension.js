
/*
 * Show Desktop from Overview
 * Very small extension which minimizes all windows when you click on an empty space in the windows-overview-mode.
 * Version for Gnome-Shell 3.6
 *
 * Changelog:
 * Version 3&4: Don't minimize icons on desktop if shown there
 *
 * Contact:
 * bazonbloch@arcor.de
 * Feel free to send improvments!
*/

const Main = imports.ui.main;
const Shell = imports.gi.Shell;

var connectid = null;
var reactiveBefore = null;

function _showDesktop() {
    //TODO: when public, do that by meta_screen_show_desktop();
    
    let activeWorkspace = global.screen.get_active_workspace();
    let tracker = Shell.WindowTracker.get_default();
    let windows = activeWorkspace.list_windows();
    for (let i = 0; i < windows.length; i++) {
           //New in V3&4: tracker.is_window_interesting checks whether this is a real window and not a desktop icon
           if (!windows[i].minimized && tracker.is_window_interesting(windows[i])) {
	       windows[i].minimize();
           }
    }
    Main.overview.hide();
}

function init() {
    reactiveBefore=Main.overview._viewSelector._workspacesDisplay.actor.reactive;
    //normal: false. but maybe set by another extension.
}

function enable() {
    Main.overview._viewSelector._workspacesDisplay.actor.reactive=true;
    connectid = Main.overview._viewSelector._workspacesDisplay.actor.connect('button-press-event', _showDesktop);
}

function disable() {
    //set back to default value which is normally false.
    Main.overview._viewSelector._workspacesDisplay.actor.reactive=reactiveBefore;
    Main.overview._viewSelector._workspacesDisplay.actor.disconnect(connectid);
}
