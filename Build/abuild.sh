#!/bin/bash
#   The aBuild script.
#   Turns Ubuntu into AriOS

# Check if script is in the working directory or not

ls | grep abuild.sh &>/dev/null
if [ $? != 0 ]; then
	echo 'Please run the script from its own directory! Aborting.'
	exit 1
fi

# Check for root privileges
if [ `id -u` !=  0 ]; then
	echo 'You need to be root to run aBuild! Aborting.'
	exit 1
fi

# Check for necessary packages
dpkg -L uck &>/dev/null
if [ $? != 0 ]; then
	echo 'ERROR: Missing dependencies! Aborting.'
	echo 'aBuild requires Ubuntu Customization Kit but it is not installed.'
	echo 'You can do it with the following command:'
	echo 'sudo apt-get install uck'
	exit 1
fi

# Debug or not
debug=false
if [ -n "$(echo $* | grep '\--debug')" ]; then
	debug=true
fi


# i386 or amd64
ARCH="i386"
if [ -n "$(echo $* | grep '\--amd64')" ]; then
	ARCH="amd64"
fi

# Check for base iso
BASE_ISO="ubuntu-12.10-desktop-$ARCH.iso"
ls $BASE_ISO &>/dev/null
if [ $? != 0 ]; then
	echo "$BASE_ISO not found! Aborting."
	exit 1;
fi


# Variables
VERSION="5.0"
BASE="Ubuntu 12.10"

# Get working directory
WORKDIR=$(pwd)

TARGET="$WORKDIR/AriOS-$VERSION-$ARCH.iso"

REMASTER_DIR="$WORKDIR/temp"

# Clean up previous build
if [ $debug == true ]; then echo '$ uck-remaster-clean "$REMASTER_DIR"'; fi
uck-remaster-clean "$REMASTER_DIR"

if [ $debug == true ]; then echo '$ rm -R $REMASTER_DIR &>/dev/null'; fi
rm -R $REMASTER_DIR &>/dev/null

if [ $debug == true ]; then echo '$ mkdir -p $REMASTER_DIR &>/dev/null'; fi
mkdir -p $REMASTER_DIR &>/dev/null

# Print AriOS logo! :)
echo '                                                                                '
echo '                                     ..  ..                                     '
echo '                                    /-    -/                                    '
echo '                                  -s.      .s-                                  '
echo '                                `os`        `ss`                                '
echo '                               :d+            /d:                               '
echo '                             .ym:              -my.                             '
echo '                            /mh.        ``.     .hm/                            '
echo '                          .hNs`     +hdmmmd      `sNh.                          '
echo '                        `+NN+       :NNNmdy        /NNo`                        '
echo '                       -hMm-       ./hdddhh`        -mMd-                       '
echo '                     `oNMh.       .NMMNMmmm-         .hMNs`                     '
echo '                    :dMMs`         +yhsdMNN+          `oMMd:                    '
echo '                  .sNMN/           `/hhhdmMd            /NMNy.                  '
echo '                 /mMMm-            oyNMNydmM`      `     -dMMm/                 '
echo '               .yMMmy.             hmMMMyMyM/  `/soo      `ymMMy.               '
echo '             `+mNh+.              `yMMMMsNhhs/:odhs:.`      ./hNN+`             '
echo '            -hdo-                 `dMMMMyydmMNo::+-.-/:        -odh-            '
echo '          `/s:`                   .dMMMMMMMMMs+os-    y          `:s+`          '
echo '         `--``````````````````````+yMMMMMMMMdy/oo/.../+`````````````--`         '
echo '         `ydddddddddddddddddddddddmNyMMMMMMNsydddmdhhmdddddddddddddddy`         '
echo '          `+dNMMMMMMMMMMMMMMMMMMMMNMhmMMMMMymyMMMMMMMMMMMMMMMMMMMMNd+`          '
echo '             -+sdmmNNNNMMMMMMMMMm/+MMsMMMMhmo:dMMMMMMMMMMNNNNNmds+-             '
echo '                  `.--::::////Nd` /MMhhMNhNm/ `yM+////::::--.`                  '
echo '                             /M.  +MMM+hdMNsh  `No                              '
echo '                             sN   oMMMdhNNyNN   dh                              '
echo '                             :M-  yMMMMmddMMM- .N+                              '
echo '                              ym. dMMMMMMMMMN/.dh`                              '
echo '                               omo:odNMMMNdo:+ms`                               '
echo '                                `odho+++++ohdo.                                 '
echo '                                   `:/+o+/:`                                    '
echo '                                                                                '

echo ""
echo "`date +%c`"
echo "Starting build of AriOS $VERSION $ARCH,"
echo "expecting $BASE as base distro:"
echo ""

# Extract the CD .iso contents
if [ $debug == true ]; then echo '$ uck-remaster-unpack-iso "$BASE_ISO" "$REMASTER_DIR"'; fi
uck-remaster-unpack-iso "$BASE_ISO" "$REMASTER_DIR"

# Extract the Desktop system
if [ $debug == true ]; then echo '$ uck-remaster-unpack-rootfs "$REMASTER_DIR"'; fi
uck-remaster-unpack-rootfs "$REMASTER_DIR"

# Unpack initial RAM disk
if [ $debug == true ]; then echo '$ uck-remaster-unpack-initrd "$REMASTER_DIR"'; fi
uck-remaster-unpack-initrd "$REMASTER_DIR"

# Make aconf.sh and root files availabe in chroot
# (UCK mounts /tmp/ of the host to the chroot)

if [ $debug == true ]; then echo '$ cp -f -t /tmp/ aconf.sh'; fi
cp -f -t /tmp/ aconf.sh
if [ $debug == true ]; then echo '$ chown root:root /tmp/aconf.sh'; fi
chown -R root:root /tmp/aconf.sh
if [ $debug == true ]; then echo '$ chmod a+x /tmp/aconf.sh'; fi
chmod a+x /tmp/aconf.sh

#if [ $debug == true ]; then echo '$ cd root'; fi
#cd root
# @TODO i think it's not so good, but i have no better idea yet.
#if [ $debug == true ]; then echo '$ chown -R root:root *'; fi
#chown -R root:root *
#if [ $debug == true ]; then echo '$ chmod -R 755 *'; fi
#chmod -R 755 *
#if [ $debug == true ]; then echo '$ tar czf ../root.tar.gz *'; fi
#tar czf ../root.tar.gz *
#if [ $debug == true ]; then echo '$ cd ..'; fi
#cd ..
#if [ $debug == true ]; then echo '$ cp -f -t /tmp/ root.tar.gz'; fi
#cp -f -t /tmp/ root.tar.gz
if [ $debug == true ]; then echo '$ cp -f -t /tmp/ root.tar'; fi
cp -f -t /tmp/ root.tar
if [ $debug == true ]; then echo '$ cp -f -t /tmp/ root-config.tar'; fi
cp -f -t /tmp/ root-config.tar

#copy deb files
mkdir -p "$WORKDIR/deb-files"
mkdir -p "$WORKDIR/deb-files/$ARCH"
mkdir -p "$WORKDIR/temp/remaster-apt-cache/archives"
mv "$WORKDIR"/deb-files/"$ARCH"/*.deb "$WORKDIR"/temp/remaster-apt-cache/archives/


# Chroot and execute aconf.sh
if [ $debug == true ]; then echo '$ uck-remaster-chroot-rootfs "$REMASTER_DIR" /tmp/aconf.sh'; fi
uck-remaster-chroot-rootfs "$REMASTER_DIR" /tmp/aconf.sh "--$ARCH"
#uck-remaster-chroot-rootfs "$REMASTER_DIR" bash

# Error handling
if [ $? != 0 ]; then
	echo 'aconf.sh returned an error. Aborting.'
	echo 'Removing temporary files...'
	# Clean up previous build

	#backing up deb files
	mkdir -p "$WORKDIR/deb-files"
	mkdir -p "$WORKDIR/deb-files/$ARCH"
	mv "$WORKDIR"/temp/remaster-apt-cache/archives/*.deb "$WORKDIR"/deb-files/"$ARCH"/
	mv "$WORKDIR"/temp/remaster-apt-cache/archives/apt-fast/*.deb "$WORKDIR"/deb-files/"$ARCH"/

	if [ $debug == true ]; then echo '$ uck-remaster-clean "$REMASTER_DIR"'; fi
	uck-remaster-clean "$REMASTER_DIR"
	if [ $debug == true ]; then echo '$ rm -R $REMASTER_DIR &>/dev/null'; fi
	rm -R $REMASTER_DIR &>/dev/null
	exit 1
fi

# Rebranding ISO contents
if [ $debug == true ]; then echo '# Rebranding ISO contents'; fi
cp -t "$REMASTER_DIR"/remaster-iso/isolinux "$WORKDIR"/rsrc/access.pcx "$WORKDIR"/rsrc/blank.pcx "$WORKDIR"/rsrc/splash.pcx "$WORKDIR"/rsrc/gfxboot.cfg
cd "$REMASTER_DIR"/remaster-iso/
sed -i 's/DISTRIB_DESCRIPTION="Ubuntu 12.10"/DISTRIB_DESCRIPTION="AriOS"/' boot/grub/loopback.cfg
sed -i "s/Ubuntu/AriOS/" boot/grub/loopback.cfg
# rm -r *.exe autorun.inf README.diskdefines pics ubuntu 
echo "AriOS $VERSION - $ARCH" > .disk/info
echo "http://arioslinux.org/news/2012/09/arios-5-0-final-is-out/" > .disk/release_notes_url
mv preseed/ubuntu.seed preseed/arios.seed 
sed -i "s/12.10/$VERSION/" isolinux/f1.txt isolinux/*.hlp
sed -i "s/Ubuntu/AriOS/" isolinux/*.hlp isolinux/txt.cfg
sed -i "s/Ubuntu/AriOS/" isolinux/f1.txt isolinux/f2.txt isolinux/f9.txt isolinux/f10.txt
sed -i "s/ Ubuntu/ AriOS/" isolinux/*.hlp isolinux/f9.txt 
sed -i "s/UBUNTU/AriOS/" isolinux/f2.txt
sed -i "s/20110427.1/`date +%Y%m%d`/" isolinux/f1.txt isolinux/*.hlp
sed -i "s/ubuntu.com/arioslinux.org/" isolinux/*.hlp isolinux/f2.txt isolinux/f9.txt
sed -i "s/Copyright/Copyleft/" isolinux/*.hlp isolinux/f10.txt
sed -i "s/ Canonical Ltd./ The AriOS team/" isolinux/*.hlp isolinux/f10.txt
sed -i "s/Debian/&, Ubuntu/" isolinux/f10.txt isolinux/*.hlp
sed -i "s/.org/&, www.ubuntu.com/" isolinux/f10.txt isolinux/*.hlp
sed -i "s/ubuntu.seed /arios.seed /" isolinux/txt.cfg boot/grub/loopback.cfg
sed -i "s/project./& Ubuntu is a registred trademark of Canonical Ltd./" isolinux/f10.txt
sed -i "s/Ubuntu /AriOS /g" isolinux/*.tr


#copy default plymoth theme
cp -r "$REMASTER_DIR"/remaster-root/lib/plymouth/themes "$REMASTER_DIR"/remaster-initrd/lib/plymouth/
sudo rm "$REMASTER_DIR"/remaster-initrd/lib/plymouth/themes/default.plymouth
sudo ln -s '/lib/plymouth/themes/AriOS/AriOS.plymouth' "$REMASTER_DIR"/remaster-initrd/lib/plymouth/themes/default.plymouth
rm -r "$REMASTER_DIR"/remaster-initrd/lib/plymouth/themes/ubuntu-logo

#remove win32 files
uck-remaster-remove-win32-files "$REMASTER_DIR"

# Compressing initial ram disk back
if [ $debug == true ]; then echo '$ uck-remaster-pack-initrd "$REMASTER_DIR"'; fi
uck-remaster-pack-initrd "$REMASTER_DIR"

# Regenerate manifest and compress filesystem
if [ $debug == true ]; then echo '$ uck-remaster-pack-rootfs "$REMASTER_DIR"'; fi
uck-remaster-pack-rootfs "$REMASTER_DIR"

# Create the ISO image
if [ $debug == true ]; then echo '$ uck-remaster-pack-iso "$TARGET" "$REMASTER_DIR" --description="AriOS-$VERSION"'; fi
uck-remaster-pack-iso "$TARGET" "$REMASTER_DIR" --description="AriOS-$VERSION"

# md5sum the image
if [ $debug == true ]; then echo '$ md5sum "$TARGET" > "$TARGET".md5'; fi
md5sum "$TARGET" > "$TARGET".md5

#backing up deb files
mkdir -p "$WORKDIR/deb-files"
mkdir -p "$WORKDIR/deb-files/$ARCH"
mv "$WORKDIR"/temp/remaster-apt-cache/archives/*.deb "$WORKDIR"/deb-files/"$ARCH"/
mv "$WORKDIR"/temp/remaster-apt-cache/archives/apt-fast/*.deb "$WORKDIR"/deb-files/"$ARCH"/

# Clean up
if [ $debug == true ]; then echo '$ uck-remaster-clean "$REMASTER_DIR"'; fi
uck-remaster-clean "$REMASTER_DIR"

if [ $debug == true ]; then echo '$ rm -R $REMASTER_DIR &>/dev/null'; fi
rm -R $REMASTER_DIR &>/dev/null

# Saying goodbye!
echo ""
echo "`date +%c`"
echo "aBuild for AriOS $VERSION ($ARCH) has finished!"
echo "You will find your new AriOS build at $TARGET"
echo ""

exit 0
